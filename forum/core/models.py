from django.db import models
from django.core.validators import MinLengthValidator


class Category(models.Model):
    """Forum category model"""
    name = models.CharField('Category.name', max_length=16, validators=[MinLengthValidator(3)])
    order_number = models.PositiveSmallIntegerField('Order number', default=0)

    class Meta:
        """Meta class
        Ustawia pewne rzeczy istotne dla Django"""
        verbose_name = 'Category'
        verbose_name_plural = 'Categories'

    def __str__(self):
        return self.name
