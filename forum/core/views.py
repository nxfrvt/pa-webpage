from django.views.generic import ListView

from .models import Category


class IndexView(ListView):
    """Index view"""
    model = Category
    template_name = "index.html"



index_view = IndexView.as_view()
